package pc.dd.testvideorecord.Fragments;

import android.content.Intent;
import android.media.projection.MediaProjectionManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.io.File;
import java.io.IOException;

import pc.dd.testvideorecord.R;
import pc.dd.testvideorecord.activity.MainActivity;
import pc.dd.testvideorecord.helpers.ChatArrayAdapter;
import pc.dd.testvideorecord.helpers.ChatMessage;
import pc.dd.testvideorecord.helpers.ScreenRecorder;

import static android.content.Context.MEDIA_PROJECTION_SERVICE;
import static android.content.Intent.FLAG_FROM_BACKGROUND;
import static android.content.Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION;
import static android.content.Intent.FLAG_GRANT_PREFIX_URI_PERMISSION;
import static android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION;
import static pc.dd.testvideorecord.activity.MainActivity.FirstUser;
import static pc.dd.testvideorecord.activity.MainActivity.createVideo;
import static pc.dd.testvideorecord.activity.MainActivity.mRecorder;
import static pc.dd.testvideorecord.activity.MainActivity.mediaProjection;
import static pc.dd.testvideorecord.activity.MainActivity.permissionIntent;

/**
 * Created by leaditteam on 26.07.17.
 */

public class LeftUserFragment extends Fragment {
    EditText editTextFirst;
    ImageView leftSmsImagePickIcon;
    Boolean lightLeft = true;
    LinearLayout layoutChangeTextColorBoxLeft;
    
    final int PICK_IMAGE_LEFT = 6543;
    final int CODE_TO_REC = 1239;
    final int CODE_TO_REC_SEND_NORMAL = 1112;
    
    ListView listView;
    ChatArrayAdapter chatArrayAdapter;
    CardView sendFirst;
    View root;
    
    public LeftUserFragment(ListView listView, ChatArrayAdapter chatArrayAdapter) {
        this.listView = listView;
        this.chatArrayAdapter = chatArrayAdapter;
    }
    
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.view_send_sms_first, container, false);
        layoutChangeTextColorBoxLeft = root.findViewById(R.id.layoutSendColorLeft);
        leftSmsImagePickIcon = (ImageView) root.findViewById(R.id.leftImagePickerSms);
        leftSmsImagePickIcon.setOnClickListener(leftImagePickerClickListener);
        //firstSmsLinearLayout = (RelativeLayout) root.findViewById(R.id.first_sms_l);
        editTextFirst = (EditText) root.findViewById(R.id.first_edittex);
        editTextFirst.addTextChangedListener(changeTextInEditTextFirst);
        sendFirst = (CardView) root.findViewById(R.id.first_send);
        sendFirst.setOnClickListener(sendSmsFirst);
        
        return root;
        
    }
    
    @Override
    public void onResume() {
        super.onResume();
        if (layoutChangeTextColorBoxLeft != null) {
            layoutChangeTextColorBoxLeft.setBackgroundColor(getResources().getColor(FirstUser.getColorUserBox()));
        }
    }
    
    View.OnClickListener leftImagePickerClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
            getIntent.setType("image/*");
            
            Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            pickIntent.setType("image/*");
            
            Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});
            
            getActivity().startActivityForResult(chooserIntent, PICK_IMAGE_LEFT);
        }
    };
    
    TextWatcher changeTextInEditTextFirst = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            try {
                if (mRecorder.isRecording()) {
                    mRecorder.quit();
                }
            } catch (Exception e) {
                Log.i("text in", "error");
            }
            ;
        }
        
        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (mediaProjection == null) {
                getActivity().startActivityForResult(permissionIntent, CODE_TO_REC);
                Log.e("@@", "media projection is null");
            } else {
                createVideo(FirstUser.getSoundTyping());
            }
            
        }
        
        @Override
        public void afterTextChanged(Editable editable) {
            
        }
    };
    
    View.OnClickListener sendSmsFirst = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            // MainActivity.this.getIntent().addFlags(FLAG_GRANT_READ_URI_PERMISSION & FLAG_FROM_BACKGROUND & FLAG_GRANT_PERSISTABLE_URI_PERMISSION & FLAG_GRANT_PREFIX_URI_PERMISSION);
            chatArrayAdapter.add(new ChatMessage(true, editTextFirst.getEditableText().toString(), "", FirstUser.getColorUserMessageText(), FirstUser.getColorUserBox(), FirstUser.getShowAvatar(), FirstUser.getAvatarBitmap()));
            editTextFirst.removeTextChangedListener(changeTextInEditTextFirst);
            editTextFirst.setText("");
            editTextFirst.addTextChangedListener(changeTextInEditTextFirst);
            listView.setSelection(chatArrayAdapter.getCount() - 1);
            if (mediaProjection == null) {
                startActivityForResult(permissionIntent, CODE_TO_REC_SEND_NORMAL);
                Log.e("@@", "media projection is null");
            } else {
                createVideo(FirstUser.getSoundSendSms());
            }
            
        }
    };
    
   
}

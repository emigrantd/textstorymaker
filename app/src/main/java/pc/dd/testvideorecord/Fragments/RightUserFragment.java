package pc.dd.testvideorecord.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import pc.dd.testvideorecord.R;
import pc.dd.testvideorecord.helpers.ChatArrayAdapter;
import pc.dd.testvideorecord.helpers.ChatMessage;

import static pc.dd.testvideorecord.activity.MainActivity.SecondUser;
import static pc.dd.testvideorecord.activity.MainActivity.createVideo;
import static pc.dd.testvideorecord.activity.MainActivity.mRecorder;
import static pc.dd.testvideorecord.activity.MainActivity.mediaProjection;
import static pc.dd.testvideorecord.activity.MainActivity.permissionIntent;

/**
 * Created by leaditteam on 26.07.17.
 */

public class RightUserFragment extends Fragment {
    EditText editTextSecond;
    ImageView rightSmsImagePickIcon;
    Boolean lightRight = true;
    LinearLayout layoutChangeTextColorBoxRight;
    final int PICK_IMAGE_RIGHT = 7654;
    final int CODE_TO_REC_SEND_NORMAL = 1112;
    final int CODE_TO_REC = 1239;
    ListView listView;
    CardView sendSecond;
    ChatArrayAdapter chatArrayAdapter;
    private View root;
    
    public RightUserFragment(ListView listView, ChatArrayAdapter chatArrayAdapter) {
        this.listView = listView;
        this.chatArrayAdapter = chatArrayAdapter;
    }
    
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.view_send_sms_second, container, false);
        layoutChangeTextColorBoxRight = root.findViewById(R.id.layoutSendColorRight);
        rightSmsImagePickIcon = (ImageView) root.findViewById(R.id.rightImagePickerSms);
        rightSmsImagePickIcon.setOnClickListener(rightSmsImagePickerOnClick);
        editTextSecond = (EditText) root.findViewById(R.id.second_edittex);
        editTextSecond.addTextChangedListener(changeTextInEditTextSecond);
        sendSecond = (CardView) root.findViewById(R.id.second_send);
        sendSecond.setOnClickListener(sendSmsSecond);
        
        return root;
    }
  
    
    
    View.OnClickListener rightSmsImagePickerOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
            getIntent.setType("image/*");
            
            Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            pickIntent.setType("image/*");
            
            Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});
    
            getActivity().startActivityForResult(chooserIntent, PICK_IMAGE_RIGHT);
        }
    };
    
    View.OnClickListener sendSmsSecond = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            chatArrayAdapter.add(new ChatMessage(false, editTextSecond.getEditableText().toString(), "", SecondUser.getColorUserMessageText(), SecondUser.getColorUserBox(), SecondUser.getShowAvatar(), SecondUser.getAvatarBitmap()));
            editTextSecond.removeTextChangedListener(changeTextInEditTextSecond);
            editTextSecond.setText("");
            editTextSecond.addTextChangedListener(changeTextInEditTextSecond);
            listView.setSelection(chatArrayAdapter.getCount() - 1);
            if (mediaProjection == null) {
                startActivityForResult(permissionIntent, CODE_TO_REC_SEND_NORMAL);
                Log.e("@@", "media projection is null");
            } else {
                createVideo(SecondUser.getSoundSendSms());
            }
        }
    };
    
    @Override
    public void onResume() {
        super.onResume();
        if (layoutChangeTextColorBoxRight != null) {
            layoutChangeTextColorBoxRight.setBackgroundColor(getResources().getColor(SecondUser.getColorUserBox()));
        }
    }
    
    TextWatcher changeTextInEditTextSecond = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            try {
                if (mRecorder.isRecording()) {
                    mRecorder.quit();
                }
            } catch (Exception e) {
                Log.i("text in", "error");
            }
            ;
        }
        
        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (mediaProjection == null) {
                getActivity().startActivityForResult(permissionIntent, CODE_TO_REC);
                Log.e("@@", "media projection is null");
            } else {
                createVideo(SecondUser.getSoundTyping());
            }
        }
        
        @Override
        public void afterTextChanged(Editable editable) {
            
        }
    };
    
}

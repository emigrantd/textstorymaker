package pc.dd.testvideorecord.activity;

import android.content.Context;
import android.content.Intent;
import android.media.MediaCodec;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.coremedia.iso.IsoFile;
import com.coremedia.iso.boxes.Container;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.FFmpegExecuteResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpegLoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AppendTrack;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import pc.dd.testvideorecord.R;
import pc.dd.testvideorecord.helpers.Constants;
import pc.dd.testvideorecord.helpers.CropEntity;
import pc.dd.testvideorecord.helpers.SoundConstant;

/**
 * Created by leaditteam on 18.07.17.
 */

public class CreateVideo extends AsyncTask<Void, Integer, Void> {
    final String TAG = "CreateVideo";
    FFmpeg ffmpeg;
    String workingDirectory;
    Context context;
    String pathToAudioFile;
    HashMap<Integer, Double> indexItteration = new HashMap<>();
    int index;
    String tempDirectory;
    String resultDirectory;
    CropEntity myCropEntity;
    ProgressBar mProgressBar;
    
    public CreateVideo(FFmpeg fFmpeg, String workingDirectory, Context context, HashMap<Integer, Double> tempIndexLetterObserver, String tempDirectory, String resultDirectory, CropEntity myCropEntity, ProgressBar progress) {
        this.ffmpeg = fFmpeg;
        this.workingDirectory = workingDirectory;
        this.context = context;
        this.indexItteration.putAll(tempIndexLetterObserver);
        this.index = indexItteration.size();
        this.tempDirectory = tempDirectory;
        this.resultDirectory = resultDirectory;
        this.myCropEntity = myCropEntity;
        this.mProgressBar = progress;
    }
    
    
    @Override
    protected Void doInBackground(Void... voids) {
        try {
            ffmpeg.loadBinary(new FFmpegLoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    Log.d("ff", "EFEFEFFE");
                }
                
                @Override
                public void onSuccess() {
                    Log.d("ss", "EFEFEFFE");
                }
                
                @Override
                public void onStart() {
                    Log.d("s", "EFEFEFFE");
                }
                
                @Override
                public void onFinish() {
                    Log.d("f", "EFEFEFFE");
                }
            });
        } catch (FFmpegNotSupportedException e) {
            e.printStackTrace();
            Log.d("EEEEEEE", e.getMessage());
        }
        String path = workingDirectory;
        File dir = new File(path);
        if (dir.mkdirs() || dir.isDirectory()) {
            String str_song_name_tap_normal = 0 + "s" + ".aac";
            String str_song_name_send_normal = 1 + "s" + ".aac";
            String str_song_name_send_silent = 2 + "s" + ".aac";
            String str_song_name_send_swipe = 3 + "s" + ".aac";
            try {
                CopyRAWtoSDCard(R.raw.keyboard_tap_my, path + File.separator + str_song_name_tap_normal);
                CopyRAWtoSDCard(R.raw.sent_1, path + File.separator + str_song_name_send_normal);
                CopyRAWtoSDCard(R.raw.sent_silent, path + File.separator + str_song_name_send_silent);
                CopyRAWtoSDCard(R.raw.swipe, path + File.separator + str_song_name_send_swipe);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        for (int i = 0; i < index; i++) { //
            String file11 = workingDirectory + File.separator + "v" + i + ".mp4";
            String file3 = workingDirectory + File.separator + "vs" + i + ".mp4";
            if(indexItteration.get(i) != SoundConstant.SOUND_SEND_SMS_SWIPE.getValue())
            addVideoDuration(file11, file3, i);
        }
        
        for (int i = 0; i < index; i++) { //
            String file11;
            if(indexItteration.get(i) != SoundConstant.SOUND_SEND_SMS_SWIPE.getValue()) {
                file11 = workingDirectory + File.separator + "vs" + i + ".mp4";
            } else {
                file11  = workingDirectory + File.separator + "v" + i + ".mp4";
            }
            String file3 = workingDirectory + File.separator + "out" + i + ".mp4";
            Log.i("FILE STORAGE", file11);
            pathToAudioFile = getPathTOAudioFile(i);
            runCreator(file11, file3, pathToAudioFile);
         
        }
        Movie[] inMovies = new Movie[index];
        String[] pathMovie = new String[index];
        for (int i = 0; i < index; i++) {
            try {
                pathMovie[i] = workingDirectory + File.separator + "out" + i + ".mp4";
                inMovies[i] = MovieCreator.build(pathMovie[i]);
                //get duration of video
            } catch (Exception e) {
                Log.i("ER", e.getMessage());
            }
        }
        try {
            List<Track> videoTracks = new LinkedList<Track>();
            List<Track> audioTracks = new LinkedList<Track>();
            int i = 0;
            for (Movie m : inMovies) {
                try {
                    for (Track t : m.getTracks()) {
                        if (t.getHandler().equals("soun")) {
                            audioTracks.add(t);
                            i++;
                        }
                        if (t.getHandler().equals("vide")) {
                            videoTracks.add(t);
                        }
                        
                    }
                    
                } catch (Exception e) {
                    Log.i("GET TRAKS ER", e.getMessage());
                }
            }
            Movie result = new Movie();
            
            if (audioTracks.size() > 0) {
                result.addTrack(new AppendTrack(audioTracks.toArray(new Track[audioTracks.size()])));
            }
            if (videoTracks.size() > 0) {
                result.addTrack(new AppendTrack(videoTracks
                        .toArray(new Track[videoTracks.size()])));
            }
            Container mp4file = new DefaultMp4Builder().build(result);
            FileChannel fc = new FileOutputStream(new File(tempDirectory)).getChannel();
            mp4file.writeContainer(fc);
            fc.close();
            
            cropVideo(tempDirectory, resultDirectory);
            //deleteDirectory(new File(workingDirectory));
        } catch (Exception e) {
            Log.i("ERROR CREATE FILE", e.toString());
        }
        return null;
    }
    
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }
    
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }
    
    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        mProgressBar.setProgress((int) values[0]);
    }
    
    @Override
    protected void onCancelled(Void aVoid) {
        super.onCancelled(aVoid);
    }
    
    @Override
    protected void onCancelled() {
        super.onCancelled();
        Toast.makeText(context, "Canceled!", Toast.LENGTH_SHORT).show();
    }
    
    private String getPathTOAudioFile(int interation) {
        if (indexItteration.get(interation) == (SoundConstant.SOUND_TAP_NORMAL.getValue())) {
            return workingDirectory + "/" + 0 + "s" + ".aac";
        } else if (indexItteration.get(interation) == (SoundConstant.SOUND_SEND_SMS_NORMAL.getValue())) {
            return workingDirectory + "/" + 1 + "s" + ".aac";
        } else if (indexItteration.get(interation) == (SoundConstant.SOUND_SILENT.getValue())) {
            return workingDirectory + "/" + 2 + "s" + ".aac";
        } else if (indexItteration.get(interation) == (SoundConstant.SOUND_SEND_SMS_SWIPE.getValue())) {
            return workingDirectory + "/" + 3 + "s" + ".aac";
        } else {
            return workingDirectory + "/" + 0 + "s" + ".aac";
        }
    }
    
    public void cropVideo(String inputFile, String outputFile) {
        onProgressUpdate(80);
        String crop = "crop=" +
                (int) myCropEntity.getWidht() + ":" +
                (int) myCropEntity.getHeight() + ":" +
                (int) myCropEntity.getX() + ":"
                + (int) myCropEntity.getY();
        File out = new File(outputFile);
        try {
            out.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        File newVideoFile = new File(outputFile);//-f h264 //-bsf:a aac_adtstoasc
        final String[] cmd = ("-y -i " + inputFile + " -c:v libx264 -vf " + crop + " -threads 5 -preset ultrafast -strict -2 " + newVideoFile.getAbsolutePath()).split(" ");
        
        try {
            
            ffmpeg.execute(cmd, new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    Log.i("succes:", message);
                    onProgressUpdate(100);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            context.startActivity(new Intent(context, PlayVideoActivity.class).putExtra(Constants.RESULT_FIDEO_URL.name(), resultDirectory));
                            ((ProgressActivity) context).finish();
                        }
                    }, 800);
                }
                
                @Override
                public void onProgress(String s) {
                    Log.d(TAG, "Started command : ffmpeg " + Arrays.toString(cmd));
                    Log.d(TAG, "progress : " + s);
                    onProgressUpdate(90);
                }
                
                @Override
                public void onFailure(String message) {
                    Log.i("failure:", message);
                    onCancelled();
                }
                
                @Override
                public void onStart() {
                    Log.i("START:", "STARTq");
                    onProgressUpdate(85);
                }
                
                @Override
                public void onFinish() {
                    Log.i("Finish:", "FINISH");
                    
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            onCancelled();
            Log.i("ERRRRROOROROROOR:", e.getMessage());
        }
    }
    
    private void runCreator(String input, String outputFile, String audioFilePath) {
        try {
            
            File file = new File(outputFile);
            file.createNewFile();
            outputFile = file.getAbsolutePath();
            
            MediaExtractor videoExtractor = new MediaExtractor();
            videoExtractor.setDataSource(input);
            
            MediaExtractor audioExtractor = new MediaExtractor();
            
            audioExtractor.setDataSource(audioFilePath);
            
            MediaMuxer muxer = new MediaMuxer(outputFile, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
            
            videoExtractor.selectTrack(0);
            MediaFormat videoFormat = videoExtractor.getTrackFormat(0);
            int videoTrack = muxer.addTrack(videoFormat);
            
            audioExtractor.selectTrack(0);
            MediaFormat audioFormat = audioExtractor.getTrackFormat(0);
            int audioTrack = muxer.addTrack(audioFormat);
            
            boolean sawEOS = false;
            int frameCount = 0;
            int offset = 100;
            int sampleSize = 356 * 1024;
            ByteBuffer videoBuf = ByteBuffer.allocate(sampleSize);
            ByteBuffer audioBuf = ByteBuffer.allocate(sampleSize);
            MediaCodec.BufferInfo videoBufferInfo = new MediaCodec.BufferInfo();
            MediaCodec.BufferInfo audioBufferInfo = new MediaCodec.BufferInfo();
            
            
            videoExtractor.seekTo(0, MediaExtractor.SEEK_TO_CLOSEST_SYNC);
            audioExtractor.seekTo(0, MediaExtractor.SEEK_TO_CLOSEST_SYNC);
            
            muxer.start();
            
            while (!sawEOS) {
                videoBufferInfo.offset = offset;
                videoBufferInfo.size = videoExtractor.readSampleData(videoBuf, offset);
                
                if (videoBufferInfo.size < 0 || audioBufferInfo.size < 0) {
                    sawEOS = true;
                    videoBufferInfo.size = 0;
                    
                } else {
                    videoBufferInfo.presentationTimeUs = videoExtractor.getSampleTime();
                    videoBufferInfo.flags = videoExtractor.getSampleFlags();
                    muxer.writeSampleData(videoTrack, videoBuf, videoBufferInfo);
                    videoExtractor.advance();
                    
                    frameCount++;
                }
            }
            
            boolean sawEOS2 = false;
            int frameCount2 = 0;
            while (!sawEOS2) {
                frameCount2++;
                
                audioBufferInfo.offset = offset;
                audioBufferInfo.size = audioExtractor.readSampleData(audioBuf, offset);
                
                if (videoBufferInfo.size < 0 || audioBufferInfo.size < 0) {
                    Log.d(TAG, "saw input EOS.");
                    sawEOS2 = true;
                    audioBufferInfo.size = 0;
                } else {
                    audioBufferInfo.presentationTimeUs = audioExtractor.getSampleTime();
                    audioBufferInfo.flags = audioExtractor.getSampleFlags();
                    muxer.writeSampleData(audioTrack, audioBuf, audioBufferInfo);
                    audioExtractor.advance();
                }
            }
            muxer.stop();
            muxer.release();
            
            onProgressUpdate(40);
        } catch (IOException e) {
            Log.d(TAG, "Mixer Error 1 " + e.getMessage());
        } catch (Exception e) {
            Log.d(TAG, "Mixer Error 2 " + e.getMessage());
        }
    }
    
    private void addVideoDuration(String pathToFirst, String pathToSecond, int iteration) {
        try {
            List<Track> videoTracks = new LinkedList<Track>();
            int sampleSize = 400 * 1024;
            ByteBuffer.allocate(sampleSize);
            Movie inMovies = MovieCreator.build(pathToFirst);
            Track lastTrack = null;
            
            double lengthInSeconds = 0;
            for (Track t : inMovies.getTracks()) {
                if (t.getHandler().equals("soun")) {
                    
                }
                if (t.getHandler().equals("vide")) {
                    lastTrack = t;
                    videoTracks.add(t);
                }
            }
            
            IsoFile isoFileFirst = new IsoFile(pathToFirst);
            lengthInSeconds = (double)
                    isoFileFirst.getMovieBox().getMovieHeaderBox().getDuration() /
                    isoFileFirst.getMovieBox().getMovieHeaderBox().getTimescale();
            Log.i("FIRST DURATION", String.valueOf(lengthInSeconds));
            
            videoTracks.clear();
            
            double mylength = (double) lastTrack.getDuration();
            while (lengthInSeconds <= indexItteration.get(iteration)) {
                try {
                    videoTracks.add(lastTrack);
                    lengthInSeconds += mylength;
                    Log.i("DURATION NORMAL > 3 ", String.valueOf((double) lastTrack.getDuration()));
                    if (lengthInSeconds == 0 || lengthInSeconds < 100) break;
                } catch (Exception e) {
                    Log.i("DURATION", "NORMAL BUT ERROROR");
                }
                
            }
            ;
            
            Movie result = new Movie();
//            CroppedTrack croppedTrack = new CroppedTrack(new AppendTrack(videoTracks
//                    .toArray(new Track[videoTracks.size()])),0,Math.round(0.3*100));
            //Log.d("CROPPED TRAK:",String.valueOf((double)croppedTrack.getDuration()) );
            if (videoTracks.size() > 0) {
                result.addTrack(new AppendTrack(videoTracks
                        .toArray(new Track[videoTracks.size()])));
            }
            Container mp4file = new DefaultMp4Builder().build(result);
            FileChannel fc = new FileOutputStream(new File(pathToSecond)).getChannel();
            mp4file.writeContainer(fc);
            fc.close();
            IsoFile isoFile = new IsoFile(pathToSecond);
            lengthInSeconds = (double)
                    isoFile.getMovieBox().getMovieHeaderBox().getDuration() /
                    isoFile.getMovieBox().getMovieHeaderBox().getTimescale();
            Log.i("SECOND DURATION", String.valueOf(lengthInSeconds));
            onProgressUpdate(20);
        } catch (Exception e) {
            e.getMessage();
        }
    }
    
    private void CopyRAWtoSDCard(int id, String path) throws IOException {
        onProgressUpdate(10);
        InputStream in = context.getResources().openRawResource(id);
        FileOutputStream out = new FileOutputStream(path);
        byte[] buff = new byte[1024];
        int read = 0;
        try {
            while ((read = in.read(buff)) > 0) {
                out.write(buff, 0, read);
            }
        } finally {
            in.close();
            out.close();
        }
    }
    
}

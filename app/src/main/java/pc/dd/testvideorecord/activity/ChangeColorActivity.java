package pc.dd.testvideorecord.activity;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import pc.dd.testvideorecord.R;
import pc.dd.testvideorecord.helpers.Constants;

/**
 * Created by leaditteam on 19.07.17.
 */

public class ChangeColorActivity extends Activity {
    ImageView leftImageView;
    ImageView rightImageView;
    AdView mAdView;
    TextView changeUserNameText;
    Boolean ifIthsFirstUser;
    ImageView[] arraysOfOval = new ImageView[10];
    int[] colors = new int[]{R.color.changeUserColor_REG,
            R.color.changeUserColor_GREEN,
           R.color.changeUserColor_PURPLE,
           R.color.changeUserColor_BLUE,
           R.color.changeUserColor_YELLOW,
           R.color.changeUserColor_GREY,
           R.color.changeUserColor_BLUE_PRITORNII,
           R.color.changeUserColorMessage_GREY,
           R.color.changeUserColorMessage_BLACK,
           R.color.changeUserColorMessage_WHITE
    };
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_user_color);
        ifIthsFirstUser = getIntent().getExtras().getBoolean(Constants.USER.name());
        initMainColorChangers();
        initOvalColors();
        initAds();
    }
    private void initAds(){
        MobileAds.initialize(getApplicationContext(),
                "ca-app-pub-1561400000155036~7296396904");
        mAdView = (AdView) findViewById(R.id.adViewBannerMain);
        mAdView.setAdListener(adListener);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }
    private void initMainColorChangers(){
        ((ImageView)findViewById(R.id.backButton)).setOnClickListener(goBack);
        changeUserNameText = findViewById(R.id.changeUserNameText);
        leftImageView = findViewById(R.id.leftpickColor);
        rightImageView = findViewById(R.id.rightPickColor);
    }
    private void initOvalColors(){
        arraysOfOval[0] = findViewById(R.id.changeSlideUserColor1);
        arraysOfOval[1] = findViewById(R.id.changeSlideUserColor2);
        arraysOfOval[2] = findViewById(R.id.changeSlideUserColor3);
        arraysOfOval[3] = findViewById(R.id.changeSlideUserColor4);
        arraysOfOval[4] = findViewById(R.id.changeSlideUserColor5);
        arraysOfOval[5] = findViewById(R.id.changeSlideUserColor6);
        arraysOfOval[6] = findViewById(R.id.changeSlideUserColor7);
        arraysOfOval[7] = findViewById(R.id.changeSlideUserColor8);
        arraysOfOval[8] = findViewById(R.id.changeSlideUserColor9);
        arraysOfOval[9] = findViewById(R.id.changeSlideUserColor10);
        if (ifIthsFirstUser){
            changeUserNameText.setText(MainActivity.FirstUser.getName());
            leftImageView.setColorFilter(getResources().getColor(MainActivity.FirstUser.getColorUserBox()));
            rightImageView.setColorFilter(getResources().getColor(MainActivity.FirstUser.getColorUserMessageText()));
        }else{
            changeUserNameText.setText(MainActivity.SecondUser.getName());
            leftImageView.setColorFilter(getResources().getColor(MainActivity.SecondUser.getColorUserBox()));
            rightImageView.setColorFilter(getResources().getColor(MainActivity.SecondUser.getColorUserMessageText()));
        }
        for (int i = 0; i<arraysOfOval.length;i++){
        setColorToShare(arraysOfOval[i], getResources().getColor(colors[i]));
            
            if (i<=6) {
                final int finalI = i;
                arraysOfOval[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        leftImageView.setColorFilter(getResources().getColor(colors[finalI]));
                        if (ifIthsFirstUser){
                            MainActivity.FirstUser.setColorUserBox(colors[finalI]);
                        }else{
                            MainActivity.SecondUser.setColorUserBox(colors[finalI]);
                        }
                    }
                });
            }else{
                final int finalI1 = i;
                arraysOfOval[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        rightImageView.setColorFilter(getResources().getColor(colors[finalI1]));
                        if (ifIthsFirstUser){
                            MainActivity.FirstUser.setColorUserMessageText(colors[finalI1]);
                        }else{
                            MainActivity.SecondUser.setColorUserMessageText(colors[finalI1]);;
                        }
                    }
                });
            }
        }
    }
    private void setColorToShare(ImageView background , int color){
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(color);
        drawable.setShape(GradientDrawable.OVAL);
        drawable.setStroke((int)1, Color.parseColor("#646464"));
        drawable.setSize((int)50, 50);
        background.setImageDrawable(drawable);
    }
    View.OnClickListener goBack = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            onBackPressed();
        }
    };
    AdListener adListener = new AdListener() {
        @Override
        public void onAdLoaded() {
            // Code to be executed when an ad finishes loading.
            Log.i("Ads", "onAdLoaded");
        }
        
        @Override
        public void onAdFailedToLoad(int errorCode) {
            // Code to be executed when an ad request fails.
            Log.i("Ads", "onAdFailedToLoad");
        }
        
        @Override
        public void onAdOpened() {
            // Code to be executed when an ad opens an overlay that
            // covers the screen.
            Log.i("Ads", "onAdOpened");
        }
        
        @Override
        public void onAdLeftApplication() {
            // Code to be executed when the user has left the app.
            Log.i("Ads", "onAdLeftApplication");
        }
        
        @Override
        public void onAdClosed() {
            // Code to be executed when when the user is about to return
            // to the app after tapping on an ad.
            Log.i("Ads", "onAdClosed");
        }
    };
}

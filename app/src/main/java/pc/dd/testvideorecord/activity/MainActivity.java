package pc.dd.testvideorecord.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import io.fabric.sdk.android.Fabric;
import pc.dd.testvideorecord.Fragments.LeftUserFragment;
import pc.dd.testvideorecord.Fragments.RightUserFragment;
import pc.dd.testvideorecord.helpers.ChatArrayAdapter;
import pc.dd.testvideorecord.helpers.ChatMessage;
import pc.dd.testvideorecord.R;
import pc.dd.testvideorecord.helpers.Constants;
import pc.dd.testvideorecord.helpers.CropEntity;
import pc.dd.testvideorecord.helpers.MySettings;
import pc.dd.testvideorecord.helpers.ScreenRecorder;
import pc.dd.testvideorecord.helpers.SoundConstant;
import pc.dd.testvideorecord.helpers.User;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;

import static pc.dd.testvideorecord.activity.ProgressActivity.MYCROPENTITY;
import static pc.dd.testvideorecord.activity.ProgressActivity.RESULTDIRECTORY;
import static pc.dd.testvideorecord.activity.ProgressActivity.TEMPDIRECTORY;
import static pc.dd.testvideorecord.activity.ProgressActivity.TEMPINDEXLETTEROBSERVER;
import static pc.dd.testvideorecord.activity.ProgressActivity.WORKINGDIRECTORY;

public class MainActivity extends FragmentActivity {
    
    private static final int CODE_TO_REC_SWIPE = 2341;
    private SharedPreferences sharedPreferences;
    
    static public User FirstUser = new User("Me",
            "",
            "",
            R.color.changeUserColorMessage_WHITE,
            R.color.changeUserColor_GREEN,
            false,
            null,
            SoundConstant.SOUND_TAP_NORMAL.getValue(),
            SoundConstant.SOUND_SEND_SMS_NORMAL.getValue());
    
    static public User SecondUser = new User("Friend",
            "",
            "",
            R.color.changeUserColorMessage_WHITE,
            R.color.changeUserColor_PURPLE,
            false,
            null,
            SoundConstant.SOUND_TAP_NORMAL.getValue(),
            SoundConstant.SOUND_SEND_SMS_NORMAL.getValue());
    
    static MySettings MySettings = new MySettings( null, true);
    
    final int PICK_IMAGE_RIGHT = 7654;
    final int CODE_TO_REC = 1239;
    final int CODE_TO_REC_SEND_NORMAL = 1112;
    final int PICK_IMAGE_LEFT = 6543;
    static int screenHeight;
    static int screenWidth;
    static int myScreenHeight;
    
    private static final int NUM_PAGES = 2;
    static private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private static Activity mActivity;
    
    RelativeLayout root;
 
    LinearLayout toolBarLayout;
    public static MediaProjection mediaProjection;
    public static HashMap<Integer, Double> tempIndexLetterObserver = new HashMap<>();
    LinearLayout mainScreenBottomLayout;
    
    static int indexFile = 0;
    ImageView mergeImageView;
    static String workingDirectory;
    String tempDirectory;
    CardView sendFirst;
    CardView sendSecond;
    ChatArrayAdapter chatArrayAdapter;
    ListView listView;
    TextView firstPerson;
    TextView secondPerson;
    
    CardView settingsButton;
    
    ImageView clearButton;
    String resultDirectory;
    
    AdView mAdView;
    
    CropEntity myCropEntity;
    LinearLayout recorderView;
    
    View mView;
    String time;
    MediaProjectionManager mMediaProjectionManager;
    public static Intent permissionIntent;
    public static ScreenRecorder mRecorder;
    private static View bottomLabelColor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        deleteDirectory(new File(getFilesDir().getAbsolutePath()));
        checkDrawOverlayPermission();
        init();
        Fabric.with(this, new Crashlytics());
        mActivity= this;
    }
    
    private void init() {
        mMediaProjectionManager = (MediaProjectionManager) getSystemService(MEDIA_PROJECTION_SERVICE);
        permissionIntent = mMediaProjectionManager.createScreenCaptureIntent();
        
        updateUserSharedPreferences();
        bottomLabelColor = findViewById(R.id.bottomLabelColor);
        mView = findViewById(R.id.surfaceView);
        recorderView = (LinearLayout) findViewById(R.id.recorderView);
        
        clearButton = (ImageView) findViewById(R.id.clearButton);
        clearButton.setOnClickListener(clearButtonOnClickListener);
        
        mPager = findViewById(R.id.containerForInputText);
        mPagerAdapter = new SlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        
        settingsButton = (CardView) findViewById(R.id.settingsCardView);
        firstPerson = (TextView) findViewById(R.id.firstPerson);
        secondPerson = (TextView) findViewById(R.id.secondPerson);
        mainScreenBottomLayout = (LinearLayout) findViewById(R.id.mainScreenBottomLayout);
        mergeImageView = (ImageView) findViewById(R.id.mergeImageView);
        listView = (ListView) findViewById(R.id.listView);
        toolBarLayout = (LinearLayout) findViewById(R.id.tollbar_layout);
        
        mergeImageView.setOnClickListener(mergeBtnClick);
        settingsButton.setOnClickListener(goToSetting);
        
        chatArrayAdapter = new ChatArrayAdapter(MainActivity.this, R.layout.left_sms);
        listView.setAdapter(chatArrayAdapter);
        
        setKayboardListener();
        
        firstPerson.setOnClickListener(firstPersonClickListener);
        secondPerson.setOnClickListener(secondPersonClickListener);
        
        MobileAds.initialize(getApplicationContext(),
                "ca-app-pub-1561400000155036~7296396904");
        mAdView = (AdView) findViewById(R.id.adViewBannerMain);
        mAdView.setAdListener(adListener);
        mAdView.setVisibility(View.GONE);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }
    
    View.OnClickListener clearButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (chatArrayAdapter != null) {
                chatArrayAdapter.clear();
                chatArrayAdapter.notifyDataSetChanged();
                listView.invalidateViews();
            }
            indexFile = 0;
            tempIndexLetterObserver.clear();
            deleteDirectory(new File(workingDirectory));
            deleteDirectory(new File(resultDirectory));
            try {
                EditText editTextFirst = mPager.getChildAt(0).findViewById(R.id.first_edittex);
                editTextFirst.setText("");
                EditText editTextSecond = mPager.getChildAt(0).findViewById(R.id.second_edittex);
                editTextSecond.setText("");
            }catch (Exception e){};
        }
    };
    
    View.OnClickListener goToSetting = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
        }
    };
    View.OnClickListener firstPersonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mediaProjection == null) {
                startActivityForResult(permissionIntent, CODE_TO_REC_SWIPE);
                Log.e("@@", "media projection is null");
            } else {
                createVideo(SoundConstant.SOUND_SEND_SMS_SWIPE.getValue());
            }
        }
    };
    View.OnClickListener secondPersonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mediaProjection == null) {
                startActivityForResult(permissionIntent, CODE_TO_REC_SWIPE);
                Log.e("@@", "media projection is null");
            } else {
                createVideo(SoundConstant.SOUND_SEND_SMS_SWIPE.getValue());
            }
         
        }
    };
    
    private void sendImageToChatFirstUser(Uri path) {
        chatArrayAdapter.add(new ChatMessage(true, "", path.toString(), FirstUser.getColorUserMessageText(), FirstUser.getColorUserBox(), FirstUser.getShowAvatar(), FirstUser.getAvatarBitmap()));
        if (mediaProjection == null) {
           startActivityForResult(permissionIntent, CODE_TO_REC_SEND_NORMAL);
            Log.e("@@", "media projection is null");
        } else {
            createVideo(FirstUser.getSoundSendSms());
        }
    }
    private void sendImageToChatSecondUser(Uri path) {
        chatArrayAdapter.add(new ChatMessage(false, "", path.toString(), SecondUser.getColorUserMessageText(), SecondUser.getColorUserBox(), SecondUser.getShowAvatar(), SecondUser.getAvatarBitmap()));
        if (mediaProjection == null) {
            startActivityForResult(permissionIntent, CODE_TO_REC_SEND_NORMAL);
            Log.e("@@", "media projection is null");
        } else {
            createVideo(SecondUser.getSoundSendSms());
        }
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        createDirectoryFiles();
        updateUi();
    }
    
    private void createDirectoryFiles() {
        time = String.valueOf(new Date().getTime());
        workingDirectory = getFilesDir().getAbsolutePath();
        tempDirectory = getFilesDir().getAbsolutePath() + File.separator + "out_temp" + time + ".mp4";
        final String MOVIEWS = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).getAbsolutePath();
        resultDirectory = MOVIEWS + File.separator + "TextStoryMaker_" + time + ".mp4";
        if (!new File(MOVIEWS).exists()) {
            new File(MOVIEWS).mkdirs();
        }
    }
    
    @Override
    protected void onStop(){
        saveSharedPreferences();
        super.onStop();
    }
    
    private void updateUserSharedPreferences() {
        sharedPreferences = getSharedPreferences(Constants.FIRST_USER_SHARED_PREF.name(), MODE_PRIVATE);
        getPreferences(sharedPreferences, FirstUser);
        
        sharedPreferences = getSharedPreferences(Constants.SECOND_USER_SHARED_PREF.name(), MODE_PRIVATE);
        getPreferences(sharedPreferences, SecondUser);
        
        sharedPreferences = getSharedPreferences(Constants.SETTINGS.name(), MODE_PRIVATE);
        getSetting(sharedPreferences);
    }
    
    private void saveSharedPreferences() {
        sharedPreferences = getSharedPreferences(Constants.FIRST_USER_SHARED_PREF.name(), MODE_PRIVATE);
        savePref(sharedPreferences.edit(), FirstUser);
        
        sharedPreferences = getSharedPreferences(Constants.SECOND_USER_SHARED_PREF.name(), MODE_PRIVATE);
        savePref(sharedPreferences.edit(), SecondUser);
        
        sharedPreferences = getSharedPreferences(Constants.SETTINGS.name(), MODE_PRIVATE);
        saveSettings(sharedPreferences.edit());
    }
    
    private void saveSettings(SharedPreferences.Editor editor) {
        try {
            String saved = saveBitmapToFile(MySettings.getBackgroundImageChatUrl());
            editor.putString(Constants.URL_TO_IMAGE_BACKGROUND_CHAT.name(), saved);
            editor.commit();
        }catch (Exception e){}
    }
    
    private void getSetting(SharedPreferences sharedPreferences) {
        try {
            String temp = sharedPreferences.getString(Constants.URL_TO_IMAGE_BACKGROUND_CHAT.name(), "");
            byte[] imageAsBytes = Base64.decode(temp.getBytes(),Base64.DEFAULT);
            MySettings.setBackgroundImageChatUrl(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
        }catch (Exception e){}
    }
    @SuppressWarnings("ReturnInsideFinallyBlock")
    private String saveBitmapToFile(Bitmap bitmap){
        String encode ="";
        ByteArrayOutputStream out = null;
        try {
            out = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            byte[] b = out.toByteArray();
            try {
                if (out != null) {
                    out.close();
                }
                encode= Base64.encodeToString(b, Base64.DEFAULT);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return encode;
    }
    private void savePref(SharedPreferences.Editor editor, User user) {
        editor.putString(Constants.NAME.name(), user.getName());
        editor.putString(Constants.AVATAR.name(), user.getAvatar());
        editor.putString(Constants.SOUND.name(), user.getSound());
        editor.putInt(Constants.COLOR_USER_MESSAGE_TEXT.name(), user.getColorUserMessageText());
        editor.putInt(Constants.COLOR_USER_BOX.name(), user.getColorUserBox());
        editor.putString(Constants.URL_TO_AVATAR.name(), saveBitmapToFile(user.getAvatarBitmap()));
        editor.putBoolean(Constants.ARE_SHOW_AVATAR.name(), user.getShowAvatar());
        editor.putFloat(Constants.SOUND_TAP.name(), (float) user.getSoundTyping());
        editor.putFloat(Constants.SOUND_SEND_SMS.name(), (float) user.getSoundSendSms());
        editor.commit();
    }
    
    private void getPreferences(SharedPreferences sharedPreferences, User user) {
        user.setName(sharedPreferences.getString(Constants.NAME.name(), user.getName()));
        user.setAvatar(sharedPreferences.getString(Constants.AVATAR.name(), user.getAvatar()));
        user.setSound(sharedPreferences.getString(Constants.SOUND.name(), user.getSound()));
        user.setColorUserMessageText(sharedPreferences.getInt(Constants.COLOR_USER_MESSAGE_TEXT.name(), user.getColorUserMessageText()));
        user.setColorUserBox(sharedPreferences.getInt(Constants.COLOR_USER_BOX.name(), user.getColorUserBox()));
    
        try {
            String urlToAvatar = sharedPreferences.getString(Constants.URL_TO_AVATAR.name(), "");
            byte[] imageAsBytes = Base64.decode(urlToAvatar.getBytes(), Base64.DEFAULT);
            user.setAvatarBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
            user.setShowAvatar(sharedPreferences.getBoolean(Constants.ARE_SHOW_AVATAR.name(), user.getShowAvatar()));
        }catch (Exception e){
            Log.i("Show avatar ex sharPref",e.getMessage() );
        }
        user.setSoundTyping((double) sharedPreferences.getFloat(Constants.SOUND_TAP.name(), (float) user.getSoundTyping()));
        user.setSoundSendSms((double) sharedPreferences.getFloat(Constants.SOUND_SEND_SMS.name(), (float) user.getSoundSendSms()));
    }
    
    private void updateUi() {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        screenHeight = metrics.heightPixels;
        screenWidth = metrics.widthPixels;
        chatArrayAdapter.updateColors(FirstUser.getColorUserBox(), FirstUser.getColorUserMessageText(), SecondUser.getColorUserBox(), SecondUser.getColorUserMessageText());
        chatArrayAdapter.notifyDataSetChanged();
        chatArrayAdapter.notifyDataSetInvalidated();
        if (firstPerson != null) {
            firstPerson.setText(FirstUser.getName());
        }
        if (secondPerson != null) {
            secondPerson.setText(SecondUser.getName());
        }
        if (MySettings.getBackgroundImageChatUrl()!=null) {
            recorderView.setBackground(new BitmapDrawable(MySettings.getBackgroundImageChatUrl()));
        }else{
            recorderView.setBackgroundColor(getResources().getColor(R.color.white));
        }
        if (MySettings.getConversationTitleOn()) {
            findViewById(R.id.titleMain).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.titleMain).setVisibility(View.GONE);
        }
        if (mPager.getCurrentItem() != 0) {
            bottomLabelColor.setBackgroundColor(mActivity.getResources().getColor(SecondUser.getColorUserBox()));
        } else {
            bottomLabelColor.setBackgroundColor(mActivity.getResources().getColor(FirstUser.getColorUserBox()));
        }
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CODE_TO_REC:
                mediaProjection = mMediaProjectionManager.getMediaProjection(resultCode, data);
                if (mediaProjection == null) {
                    Log.e("@@", "media projection is null");
                    mediaProjection = mMediaProjectionManager.getMediaProjection(Activity.RESULT_OK, (Intent) permissionIntent.clone());
                    return;
                } else {
                    createVideo(FirstUser.getSoundTyping());
                }
                return;
            case CODE_TO_REC_SEND_NORMAL:
                mediaProjection = mMediaProjectionManager.getMediaProjection(resultCode, data);
                if (mediaProjection == null) {
                    Log.e("@@", "media projection is null");
                    return;
                } else {
                    createVideo(FirstUser.getSoundSendSms());
                }
                return;
            case CODE_TO_REC_SWIPE:
                mediaProjection = mMediaProjectionManager.getMediaProjection(resultCode, data);
                if (mediaProjection == null) {
                    Log.e("@@", "media projection is null");
                    return;
                } else {
                    createVideo(SoundConstant.SOUND_SEND_SMS_SWIPE.getValue());
                }
                return;
            case PICK_IMAGE_LEFT:
                try {
                    Uri uriLeft = data.getData();
                    if (uriLeft != null){}
                       sendImageToChatFirstUser(uriLeft);
                } catch (Exception e) {
                }
                ;
                return;
            case PICK_IMAGE_RIGHT:
                try {
                    Uri uriRight = data.getData();
                    if (uriRight != null){}
                        sendImageToChatSecondUser(uriRight);
                } catch (Exception e) {
                }
                ;
                return;
            default:
                return;
            
        }
    }
    
    public void checkDrawOverlayPermission() {
        checkIfWeCanReadNadWrite();
        /** check if we already  have permission to draw over other apps */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (!Settings.canDrawOverlays(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, 1238);
            }
            this.requestPermissions(new String[]{Manifest.permission.SYSTEM_ALERT_WINDOW}, 1234);
        }
    }
    
    private void checkIfWeCanReadNadWrite() {
        if (((ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) && ((ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) && ((ContextCompat.checkSelfPermission(this, Manifest.permission.MANAGE_DOCUMENTS) == PackageManager.PERMISSION_GRANTED))) {
            Log.v(Constants.TAG.name(), "Permission is granted");
            //File write logic here
            
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.MANAGE_DOCUMENTS}, 1231);
        }
    }
    
    @SuppressWarnings("unchecked")
    public static void createVideo(double sound) {
        File f = new File(workingDirectory);
        if (!f.exists()) {
            f.mkdirs();
        }

        final int width = screenWidth;
        final int height = screenHeight;
        final File file = new File(workingDirectory,
                "v" + indexFile + ".mp4");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Log.i("FILE STORAGE", getFilesDir() + "v" + indexFile + ".mp4" + "   O R   " + file.getAbsolutePath());
        final int bitrate = 4000;//6000000
        int successItteration = getIteration(sound);
        mRecorder = new ScreenRecorder(width, height, bitrate, 1, mediaProjection, file.getAbsolutePath(), myScreenHeight, successItteration);
    
        mRecorder.start();
        if (sound == SoundConstant.SOUND_SEND_SMS_SWIPE.getValue()){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mPager.getCurrentItem() == 0) {
                        mPager.setCurrentItem(1);
                        bottomLabelColor.setBackgroundColor(mActivity.getResources().getColor(SecondUser.getColorUserBox()));
                    }
                    else {
                        mPager.setCurrentItem(0);
                        bottomLabelColor.setBackgroundColor(mActivity.getResources().getColor(FirstUser.getColorUserBox()));
                    }
                    
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mRecorder.quit();
                        }
                    },500);
                }
            },300);
           
        }
        tempIndexLetterObserver.put(indexFile, sound);
        indexFile++;
    }

    static private int getIteration(double sound) {
        if (sound == SoundConstant.SOUND_TAP_NORMAL.getValue()) {
            return 3;
        } else if (sound == SoundConstant.SOUND_SEND_SMS_NORMAL.getValue()) {
            return 4;
        } else if (sound == SoundConstant.SOUND_SEND_SMS_SWIPE.getValue()) {
            return 8;
        } else if (sound == SoundConstant.SOUND_SILENT.getValue()) {
            return 4;
        } else {
            return 3;
        }
    }
    
    View.OnClickListener mergeBtnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mediaProjection != null)
                mediaProjection.stop();
            
            Intent progressActivity = new Intent(MainActivity.this, ProgressActivity.class);
            progressActivity.putExtra(WORKINGDIRECTORY, workingDirectory);
            progressActivity.putExtra(TEMPINDEXLETTEROBSERVER, tempIndexLetterObserver);
            progressActivity.putExtra(TEMPDIRECTORY, tempDirectory);
            progressActivity.putExtra(RESULTDIRECTORY, resultDirectory);
            progressActivity.putExtra(MYCROPENTITY, myCropEntity);
            startActivity(progressActivity);
            
        }
    };
    
    private void setKayboardListener() {
        final View activityRootView = findViewById(R.id.root);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
                if (heightDiff > dpToPx(MainActivity.this, 200)) { // if more than 200 dp, it's probably a keyboard...
                    bottomLabelColor.setVisibility(View.VISIBLE);
                    int[] locations = new int[2];
                    recorderView.getLocationInWindow(locations);
                    myCropEntity = new CropEntity(recorderView.getMeasuredWidth(), recorderView.getMeasuredHeight(), locations[0], locations[1]);
                    mView.getLayoutParams().width = (int) myCropEntity.getWidht();
                    mView.getLayoutParams().height = (int) myCropEntity.getHeight();
                    mView.setX(myCropEntity.getX());
                    mView.setY(myCropEntity.getY());
                    
                    myScreenHeight = screenHeight - getRelativeTop(mPager);
                    mainScreenBottomLayout.setVisibility(View.GONE);
                    mAdView.setVisibility(View.GONE);
                    
                    listView.setSelection(chatArrayAdapter.getCount() - 1);
                } else {
                    bottomLabelColor.setVisibility(View.GONE);
                    mainScreenBottomLayout.setVisibility(View.VISIBLE);
                    mAdView.setVisibility(View.VISIBLE);
                    
                    listView.setSelection(chatArrayAdapter.getCount() - 1);
                }
            }
        });
    }
    
    ;
    
    private int getRelativeTop(View myView) {
        if (myView.getParent() == myView.getRootView())
            return myView.getTop();
        else
            return myView.getTop() + getRelativeTop((View) myView.getParent());
    }
    
    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }
    
    AdListener adListener = new AdListener() {
        @Override
        public void onAdLoaded() {
            // Code to be executed when an ad finishes loading.
            mAdView.setVisibility(View.VISIBLE);
            Log.i("Ads", "onAdLoaded");
        }
        
        @Override
        public void onAdFailedToLoad(int errorCode) {
            // Code to be executed when an ad request fails.
            mAdView.setVisibility(View.GONE);
            Log.i("Ads", "onAdFailedToLoad");
        }
        
        @Override
        public void onAdOpened() {
            // Code to be executed when an ad opens an overlay that
            // covers the screen.
            Log.i("Ads", "onAdOpened");
        }
        
        @Override
        public void onAdLeftApplication() {
            // Code to be executed when the user has left the app.
            mAdView.setVisibility(View.GONE);
            Log.i("Ads", "onAdLeftApplication");
        }
        
        @Override
        public void onAdClosed() {
            // Code to be executed when when the user is about to return
            // to the app after tapping on an ad.
            mAdView.setVisibility(View.GONE);
            Log.i("Ads", "onAdClosed");
        }
    };
    
    public static boolean deleteDirectory(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();
            if (files == null) {
                return true;
            }
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return (path.delete());
    }
    private class SlidePagerAdapter extends FragmentPagerAdapter {
        public SlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }
    
        @Override
        public Fragment getItem(int position) {
            if (position == 0)
            return new LeftUserFragment(listView,chatArrayAdapter);
            else return new RightUserFragment(listView,chatArrayAdapter);
        }
    
        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
}

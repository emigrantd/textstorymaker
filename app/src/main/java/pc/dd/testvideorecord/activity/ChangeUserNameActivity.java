package pc.dd.testvideorecord.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import pc.dd.testvideorecord.R;
import pc.dd.testvideorecord.helpers.Constants;

/**
 * Created by leaditteam on 12.07.17.
 */

public class ChangeUserNameActivity extends Activity {
    EditText changeUserName;
    Boolean ifIthsFirstUser;
    AdView mAdView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_user_name);
        init();
    }
    private void init(){
        ((ImageView)findViewById(R.id.backButton)).setOnClickListener(goBack);
        ifIthsFirstUser = getIntent().getExtras().getBoolean(Constants.USER.name());
        changeUserName = findViewById(R.id.changeUserNameEditText);
        changeUserName.addTextChangedListener(changeUserNameEditTextOnClick);
        if (ifIthsFirstUser){
            changeUserName.setText(MainActivity.FirstUser.getName());
        }else{
            changeUserName.setText(MainActivity.SecondUser.getName());
        }
        changeUserName.setSelection(changeUserName.length());
    
        MobileAds.initialize(getApplicationContext(),
                "ca-app-pub-1561400000155036~7296396904");
        mAdView = (AdView) findViewById(R.id.adViewBannerMain);
        mAdView.setAdListener(adListener);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }
    TextWatcher changeUserNameEditTextOnClick = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        
        }
    
        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        
        }
    
        @Override
        public void afterTextChanged(Editable editable) {
            if (ifIthsFirstUser){
                MainActivity.FirstUser.setName(changeUserName.getText().toString());;
            }else{
                MainActivity.SecondUser.setName(changeUserName.getText().toString());;
            }
        }
    };
    View.OnClickListener goBack = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            onBackPressed();
        }
    };
    AdListener adListener = new AdListener() {
        @Override
        public void onAdLoaded() {
            // Code to be executed when an ad finishes loading.
            Log.i("Ads", "onAdLoaded");
        }
        
        @Override
        public void onAdFailedToLoad(int errorCode) {
            // Code to be executed when an ad request fails.
            Log.i("Ads", "onAdFailedToLoad");
        }
        
        @Override
        public void onAdOpened() {
            // Code to be executed when an ad opens an overlay that
            // covers the screen.
            Log.i("Ads", "onAdOpened");
        }
        
        @Override
        public void onAdLeftApplication() {
            // Code to be executed when the user has left the app.
            Log.i("Ads", "onAdLeftApplication");
        }
        
        @Override
        public void onAdClosed() {
            // Code to be executed when when the user is about to return
            // to the app after tapping on an ad.
            Log.i("Ads", "onAdClosed");
        }
    };
}

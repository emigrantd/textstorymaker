package pc.dd.testvideorecord.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.HashMap;

import pc.dd.testvideorecord.R;
import pc.dd.testvideorecord.helpers.CropEntity;

/**
 * Created by leaditteam on 18.07.17.
 */

public class ProgressActivity extends Activity {
   
    static String WORKINGDIRECTORY = "WORKINGDIRECTIRY";
    static String TEMPINDEXLETTEROBSERVER = "tempIndexLetterObserver";
    static String TEMPDIRECTORY = "tempDirectory";
    static String RESULTDIRECTORY = "resultDirectory";
    static String MYCROPENTITY = "myCropEntity";
    FFmpeg ffmpeg;
    private InterstitialAd mInterstitialAd;
    CreateVideo createVideo;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress);
        ffmpeg = FFmpeg.getInstance(this);
        Intent extra = getIntent();
        
        String workingDirectory = extra.getStringExtra(WORKINGDIRECTORY);
        HashMap<Integer,Double> tempIndexLetterObserver = (HashMap<Integer, Double>) extra.getSerializableExtra(TEMPINDEXLETTEROBSERVER);
        String tempDirectory = extra.getStringExtra(TEMPDIRECTORY);
        String resultDirectory = extra.getStringExtra(RESULTDIRECTORY);
        CropEntity myCropEntity = extra.getParcelableExtra(MYCROPENTITY);
        ProgressBar progressBar = findViewById(R.id.progressBar);
        createVideo = new CreateVideo(ffmpeg, workingDirectory, ProgressActivity.this, tempIndexLetterObserver, tempDirectory, resultDirectory, myCropEntity, progressBar);
    
        TextView back = findViewById(R.id.progressTextBack);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            onBackPressed();
            }
        });
        mInterstitialAd = new InterstitialAd(this);//ca-app-pub-1561400000155036~7296396904
        mInterstitialAd.setAdUnitId("ca-app-pub-1561400000155036/2726596502");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                Log.i("Ads", "onAdLoaded");
                mInterstitialAd.show();
            }
        
            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                Log.i("Ads", "onAdFailedToLoad");
                createVideo.execute();
            }
        
            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
                Log.i("Ads", "onAdOpened");
            }
        
            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
                Log.i("Ads", "onAdLeftApplication");
                createVideo.execute();
            }
        
            @Override
            public void onAdClosed() {
                // Code to be executed when when the interstitial ad is closed.
                Log.i("Ads", "onAdClosed");
                createVideo.execute();
            }
        });
    }
}

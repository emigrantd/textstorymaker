package pc.dd.testvideorecord.activity;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.widget.MediaController;
import android.widget.VideoView;

import java.io.File;

import pc.dd.testvideorecord.R;
import pc.dd.testvideorecord.helpers.Constants;

/**
 * Created by leaditteam on 12.07.17.
 */

public class PlayVideoActivity extends Activity {
    
    private String resultUrl;
    VideoView videoView;
    MediaController mediaController;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_video);
        // Grabs a reference to the player view
        
        resultUrl = getIntent().getExtras().getString(Constants.RESULT_FIDEO_URL.name());
        // Grabs a reference to the player view
        videoView = (VideoView) findViewById(R.id.videoView);
        
        videoView.setVideoPath(new File(resultUrl).getAbsolutePath());
        mediaController = new
                MediaController(this){
                    @Override
                    public void hide() {
                        mediaController.show();
                    }
                };
        mediaController.setAnchorView(videoView);
        
        videoView.setMediaController(mediaController);
        videoView.start();
        
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        
    }
}

package pc.dd.testvideorecord.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.io.InputStream;

import info.hoang8f.android.segmented.SegmentedGroup;
import pc.dd.testvideorecord.R;
import pc.dd.testvideorecord.helpers.Constants;
import pc.dd.testvideorecord.helpers.SoundConstant;

/**
 * Created by leaditteam on 12.07.17.
 */

public class SettingsActivity extends Activity {
    TextView changeUserName;
    Boolean ifFirstUser = true;
    SegmentedGroup changeUser;
    AdView mAdView;
    RelativeLayout adsRelativeLayoutSettings;
    
    ImageView changeUserColorImageActivity;
    ImageView setBackgroundButtinPicker;
    ImageView sendSoundButton;
    Switch switchBackgroundImage;
    Switch switchSoundTyping;
    Switch conversationTitle;
    final int PICK_IMAGE = 3333;
    final int PICK_IMAGE_LEFT_SMS = 3332;
    final int PICK_IMAGE_RIGHT_SMS = 3331;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        
        init();
    }
    
    private void init() {
        conversationTitle = findViewById(R.id.conversationTitle);
        conversationTitle.setOnCheckedChangeListener(conversationTitleCkeckedListener);
        switchSoundTyping = findViewById(R.id.switchSoundTyping);
        switchSoundTyping.setOnCheckedChangeListener(switchSoundTypingCheckListener);
        sendSoundButton = findViewById(R.id.sendSoundButton);
        sendSoundButton.setOnClickListener(setSoundButtonOnClickListener);
        switchBackgroundImage = findViewById(R.id.switchBackgroundImage);
        switchBackgroundImage.setOnCheckedChangeListener(chackedChangeListenerBackgroundImage);
        setBackgroundButtinPicker = findViewById(R.id.setBackgroundButtinPicker);
        setBackgroundButtinPicker.setOnClickListener(setBackgroundButtonPickerOnClick);
        changeUserColorImageActivity = findViewById(R.id.changeUserColorImageActivityBtn);
        changeUserColorImageActivity.setOnClickListener(changeUserColorImageActivityOnClickListener);
        ((ImageView) findViewById(R.id.backButton)).setOnClickListener(goBack);
        adsRelativeLayoutSettings = findViewById(R.id.adsRelativeLayoutSettings);
        adsRelativeLayoutSettings.setVisibility(View.GONE);
        changeUserName = findViewById(R.id.changeUserNameTextView);
        changeUserName.setOnClickListener(changeUserNameOnClick);
        changeUser = findViewById(R.id.changeUserSettings);
        changeUser.setOnCheckedChangeListener(checkedChangeListener);
        ((RadioButton) findViewById(R.id.firstRadioButton)).setChecked(true);
        
        MobileAds.initialize(getApplicationContext(),
                "ca-app-pub-1561400000155036~7296396904");
        mAdView = (AdView) findViewById(R.id.adViewBannerMain);
        mAdView.setAdListener(adListener);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }
    View.OnClickListener setSoundButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (ifFirstUser) {
                if (MainActivity.FirstUser.getSoundTyping() == SoundConstant.SOUND_TAP_NORMAL.getValue()) {
                    sendSoundButton.setImageDrawable(getResources().getDrawable(R.drawable.off));
                    MainActivity.FirstUser.setSoundTyping(SoundConstant.SOUND_SILENT.getValue());
                } else {
                    sendSoundButton.setImageDrawable(getResources().getDrawable(R.drawable.on));
                    MainActivity.FirstUser.setSoundTyping(SoundConstant.SOUND_TAP_NORMAL.getValue());
                }
            } else if (!ifFirstUser) {
                if (MainActivity.SecondUser.getSoundTyping() == SoundConstant.SOUND_TAP_NORMAL.getValue()) {
                    sendSoundButton.setImageDrawable(getResources().getDrawable(R.drawable.off));
                    MainActivity.SecondUser.setSoundTyping(SoundConstant.SOUND_SILENT.getValue());
                } else {
                    sendSoundButton.setImageDrawable(getResources().getDrawable(R.drawable.on));
                    MainActivity.SecondUser.setSoundTyping(SoundConstant.SOUND_TAP_NORMAL.getValue());
                }
            }
            
        }
    };
    CompoundButton.OnCheckedChangeListener conversationTitleCkeckedListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if(b){
            conversationTitle.setChecked(true);
            MainActivity.MySettings.setConversationTitleOn(true);
        }else{
            conversationTitle.setChecked(false);
            MainActivity.MySettings.setConversationTitleOn(false);
        }
            
        }
    };
    CompoundButton.OnCheckedChangeListener chackedChangeListenerBackgroundImage = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if (b) {
                Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                getIntent.setType("image/*");
                
                Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");
                
                Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});
                
                startActivityForResult(chooserIntent, PICK_IMAGE);
            }else{
                MainActivity.MySettings.setBackgroundImageChatUrl(null);
            }
        }
    };
    CompoundButton.OnCheckedChangeListener switchSoundTypingCheckListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if (!b){
                MainActivity.FirstUser.setSoundSendSms(SoundConstant.SOUND_SILENT.getValue());
                MainActivity.SecondUser.setSoundSendSms(SoundConstant.SOUND_SILENT.getValue());
            }else {
                MainActivity.FirstUser.setSoundSendSms(SoundConstant.SOUND_SEND_SMS_NORMAL.getValue());
                MainActivity.SecondUser.setSoundSendSms(SoundConstant.SOUND_SEND_SMS_NORMAL.getValue());
            }
        }
    };
    View.OnClickListener setBackgroundButtonPickerOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
            getIntent.setType("image/*");
            getIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            
            Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            pickIntent.setType("image/*");
            pickIntent.addCategory(Intent.CATEGORY_OPENABLE);
            pickIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            
            Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});
            switch (changeUser.getCheckedRadioButtonId()) {
                case R.id.firstRadioButton:
                    startActivityForResult(chooserIntent, PICK_IMAGE_LEFT_SMS);
                    return;
                case R.id.secondRadioButton:
                    startActivityForResult(chooserIntent, PICK_IMAGE_RIGHT_SMS);
                    return;
                default:
                    startActivityForResult(chooserIntent, PICK_IMAGE_LEFT_SMS);
                    return;
            }
            
        }
    };
    View.OnClickListener changeUserColorImageActivityOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(SettingsActivity.this, ChangeColorActivity.class).putExtra(Constants.USER.name(), ifFirstUser));
        }
    };
    AdListener adListener = new AdListener() {
        @Override
        public void onAdLoaded() {
            // Code to be executed when an ad finishes loading.
            adsRelativeLayoutSettings.setVisibility(View.VISIBLE);
            Log.i("Ads", "onAdLoaded");
        }
        
        @Override
        public void onAdFailedToLoad(int errorCode) {
            // Code to be executed when an ad request fails.
            adsRelativeLayoutSettings.setVisibility(View.GONE);
            Log.i("Ads", "onAdFailedToLoad");
        }
        
        @Override
        public void onAdOpened() {
            // Code to be executed when an ad opens an overlay that
            // covers the screen.
            Log.i("Ads", "onAdOpened");
        }
        
        @Override
        public void onAdLeftApplication() {
            // Code to be executed when the user has left the app.
            adsRelativeLayoutSettings.setVisibility(View.GONE);
            Log.i("Ads", "onAdLeftApplication");
        }
        
        @Override
        public void onAdClosed() {
            // Code to be executed when when the user is about to return
            // to the app after tapping on an ad.
            adsRelativeLayoutSettings.setVisibility(View.GONE);
            Log.i("Ads", "onAdClosed");
        }
    };
    RadioGroup.OnCheckedChangeListener checkedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
            switch (i) {
                case R.id.firstRadioButton:
                    ifFirstUser = true;
                    changeUserName.setText(MainActivity.FirstUser.getName());
                    if (MainActivity.FirstUser.getSoundTyping() != SoundConstant.SOUND_TAP_NORMAL.getValue()) {
                        sendSoundButton.setImageDrawable(getResources().getDrawable(R.drawable.off));
                    } else {
                        sendSoundButton.setImageDrawable(getResources().getDrawable(R.drawable.on));
                    }
                    if (MainActivity.FirstUser.getSoundSendSms() != SoundConstant.SOUND_SEND_SMS_NORMAL.getValue()){
                        switchSoundTyping.setChecked(false);
                    }else {
                        switchSoundTyping.setChecked(true);
                    }
                    return;
                case R.id.secondRadioButton:
                    ifFirstUser = false;
                    changeUserName.setText(MainActivity.SecondUser.getName());
                    if (MainActivity.SecondUser.getSoundTyping() != SoundConstant.SOUND_TAP_NORMAL.getValue()) {
                        sendSoundButton.setImageDrawable(getResources().getDrawable(R.drawable.off));
                    } else {
                        sendSoundButton.setImageDrawable(getResources().getDrawable(R.drawable.on));
                    }
                    return;
                default:
                    ifFirstUser = true;
                    changeUserName.setText(MainActivity.FirstUser.getName());
                    if (MainActivity.FirstUser.getSoundTyping() != SoundConstant.SOUND_TAP_NORMAL.getValue()) {
                        sendSoundButton.setImageDrawable(getResources().getDrawable(R.drawable.off));
                    } else {
                        sendSoundButton.setImageDrawable(getResources().getDrawable(R.drawable.on));
                    }
                    return;
            }
        }
    };
    View.OnClickListener changeUserNameOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(SettingsActivity.this, ChangeUserNameActivity.class).putExtra(Constants.USER.name(), ifFirstUser));
        }
    };
    
    @Override
    protected void onResume() {
        super.onResume();
        if (changeUser != null) {
            ((RadioButton) findViewById(R.id.firstRadioButton)).setText(MainActivity.FirstUser.getName());
            ((RadioButton) findViewById(R.id.secondRadioButton)).setText(MainActivity.SecondUser.getName());
        }
        
        if( MainActivity.MySettings.getConversationTitleOn()){
            conversationTitle.setChecked(true);
        }else{
            conversationTitle.setChecked(false);
        }
        if (MainActivity.MySettings.getBackgroundImageChatUrl()==null){
            switchBackgroundImage.setChecked(false);
        }
        switch (changeUser.getCheckedRadioButtonId()) {
            case R.id.firstRadioButton:
                ifFirstUser = true;
                changeUserName.setText(MainActivity.FirstUser.getName());
                return;
            case R.id.secondRadioButton:
                ifFirstUser = false;
                changeUserName.setText(MainActivity.SecondUser.getName());
                return;
            default:
                ifFirstUser = true;
                changeUserName.setText(MainActivity.FirstUser.getName());
                return;
        }
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PICK_IMAGE:
                if (resultCode == RESULT_OK) {
                    try {
                        Uri uriRight = data.getData();
                        if (uriRight != null) {
                            InputStream inputStream = getContentResolver().openInputStream(uriRight);
                            Bitmap bm = BitmapFactory.decodeStream(inputStream);
                            MainActivity.MySettings.setBackgroundImageChatUrl(bm);
                        }
                        
                    } catch (Exception e) {
                    }
                    ;
                } else if (resultCode == RESULT_CANCELED) {
                    switchBackgroundImage.setChecked(false);
                }
                return;
            case PICK_IMAGE_LEFT_SMS:
                if (resultCode == RESULT_OK) {
                    try {
                        Uri uriRight = data.getData();
                        if (uriRight != null)
                            MainActivity.FirstUser.setShowAvatar(true);
                        InputStream input = this.getContentResolver().openInputStream(uriRight);
                        Bitmap b = BitmapFactory.decodeStream(input);
                        MainActivity.FirstUser.setAvatarBitmap(b);
                        input.close();
                    } catch (Exception e) {
                    }
                    ;
                }
                return;
            case PICK_IMAGE_RIGHT_SMS:
                if (resultCode == RESULT_OK) {
                    try {
                        Uri uriRight = data.getData();
                        if (uriRight != null)
                            MainActivity.SecondUser.setShowAvatar(true);
                        InputStream input = this.getContentResolver().openInputStream(uriRight);
                        Bitmap b = BitmapFactory.decodeStream(input);
                        MainActivity.SecondUser.setAvatarBitmap(b);
                        input.close();
                    } catch (Exception e) {
                    }
                    ;
                }
                return;
            default:
                return;
            
        }
    }
    
    View.OnClickListener goBack = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            onBackPressed();
        }
    };
}

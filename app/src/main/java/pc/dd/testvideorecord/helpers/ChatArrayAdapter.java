package pc.dd.testvideorecord.helpers;

/**
 * Created by leaditteam on 11.07.17.
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import pc.dd.testvideorecord.R;


/**
 * Created by UserData on 19.08.2016.
 */

public class ChatArrayAdapter extends ArrayAdapter<ChatMessage> {
    
    private TextView chatText;
    private List<ChatMessage> chatMessageList = new ArrayList<ChatMessage>();
    
    @Override
    public void add(ChatMessage object) {
        chatMessageList.add(object);
        super.add(object);
    }
    
    public void updateColors(int colorBoxLeft, int colorMsgLeft, int colorBoxRight, int colorMsgRight) {
        for (int i = 0; i < chatMessageList.size(); i++) {
            if (chatMessageList.get(i).left) {
                chatMessageList.get(i).colorUserBox = colorBoxLeft;
                chatMessageList.get(i).colorUserMessageText = colorMsgLeft;
            } else {
                chatMessageList.get(i).colorUserBox = colorBoxRight;
                chatMessageList.get(i).colorUserMessageText = colorMsgRight;
            }
        }
    }
    
    public ChatArrayAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }
    
    public int getCount() {
        return this.chatMessageList.size();
    }
    
    public ChatMessage getItem(int index) {
        return this.chatMessageList.get(index);
    }
    
    public View getView(int position, final View convertView, ViewGroup parent) {
        ChatMessage chatMessageObj = getItem(position);
        View row = convertView;
        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        
        
        if (chatMessageObj.left) {
            row = inflater.inflate(R.layout.left_sms, parent, false);
            
            LinearLayout imageViewLeftSms = row.findViewById(R.id.imageViewLeftSms);
            LinearLayout textSms = row.findViewById(R.id.chatRightLayoutBkg);
            de.hdodenhof.circleimageview.CircleImageView circleImageViewAvatarImage = row.findViewById(R.id.leftSmsAvatarImage);
            de.hdodenhof.circleimageview.CircleImageView circleImageViewAvatarText = row.findViewById(R.id.leftSmsAvatarText);
            if (!chatMessageObj.image_url.equals("")) {
                circleImageViewAvatarText.setVisibility(View.GONE);
                circleImageViewAvatarImage.setVisibility(View.VISIBLE);
                imageViewLeftSms.setVisibility(View.VISIBLE);
                textSms.setVisibility(View.GONE);
                PorterShapeImageView imagePickerLeftSms = row.findViewById(R.id.imagePickerLeftSms);
                imagePickerLeftSms.setImageURI(Uri.parse(chatMessageObj.image_url));
            } else {
                circleImageViewAvatarText.setVisibility(View.VISIBLE);
                circleImageViewAvatarImage.setVisibility(View.GONE);
                textSms.setVisibility(View.VISIBLE);
                imageViewLeftSms.setVisibility(View.GONE);
                chatText = (TextView) row.findViewById(R.id.text_sms);
                chatText.setText(chatMessageObj.message);
                chatText.setTextColor(row.getResources().getColor(chatMessageObj.colorUserMessageText));
                
                ImageView leftSmsBackgroundImage = row.findViewById(R.id.leftSmsBackgroundImage);
                ImageView leftPimp = row.findViewById(R.id.leftPimp);
                leftSmsBackgroundImage.setColorFilter(row.getResources().getColor(chatMessageObj.colorUserBox));
                leftPimp.setColorFilter(row.getResources().getColor(chatMessageObj.colorUserBox));
            }
            if (chatMessageObj.isShowAvatar) {
                try {
                    circleImageViewAvatarImage.setImageBitmap(chatMessageObj.avatarBitmap);
                    circleImageViewAvatarText.setImageBitmap(chatMessageObj.avatarBitmap);
                }catch (Exception e){};
            } else {
                circleImageViewAvatarImage.setVisibility(View.GONE);
                circleImageViewAvatarText.setVisibility(View.GONE);
            }
            
        } else {
            row = inflater.inflate(R.layout.right_sms, parent, false);
            
            LinearLayout imageViewRightSms = row.findViewById(R.id.ImageViewRightSms);
            LinearLayout textSms = row.findViewById(R.id.chatRightLayoutBkg);
            de.hdodenhof.circleimageview.CircleImageView circleImageViewAvatarImage = row.findViewById(R.id.rightSmsAvatarImage);
            de.hdodenhof.circleimageview.CircleImageView circleImageViewAvatarText = row.findViewById(R.id.rightSmsAvatarText);
            if (!chatMessageObj.image_url.equals("")) {
                circleImageViewAvatarText.setVisibility(View.GONE);
                circleImageViewAvatarImage.setVisibility(View.VISIBLE);
                imageViewRightSms.setVisibility(View.VISIBLE);
                textSms.setVisibility(View.GONE);
                PorterShapeImageView imagePickerRightSms = row.findViewById(R.id.imagePickerRightSms);
                imagePickerRightSms.setImageURI(Uri.parse(chatMessageObj.image_url));
            } else {
                circleImageViewAvatarText.setVisibility(View.VISIBLE);
                circleImageViewAvatarImage.setVisibility(View.GONE);
                textSms.setVisibility(View.VISIBLE);
                imageViewRightSms.setVisibility(View.GONE);
                chatText = (TextView) row.findViewById(R.id.text_sms);
                chatText.setText(chatMessageObj.message);
                chatText.setTextColor(row.getResources().getColor(chatMessageObj.colorUserMessageText));
                
                ImageView rithtSmsBackgroundImage = row.findViewById(R.id.rightSmsBackgroundImage);
                ImageView rithgPimp = row.findViewById(R.id.rightPimp);
                rithtSmsBackgroundImage.setColorFilter(row.getResources().getColor(chatMessageObj.colorUserBox));
                rithgPimp.setColorFilter(row.getResources().getColor(chatMessageObj.colorUserBox));
            }
            if (chatMessageObj.isShowAvatar) {
                try {
                    circleImageViewAvatarImage.setImageBitmap(chatMessageObj.avatarBitmap);
                    circleImageViewAvatarText.setImageBitmap(chatMessageObj.avatarBitmap);
                }catch (Exception e){};
            } else {
                circleImageViewAvatarImage.setVisibility(View.GONE);
                circleImageViewAvatarText.setVisibility(View.GONE);
            }
        }
        
        return row;
    }
    
    @Override
    public void clear() {
        chatMessageList.clear();
        super.clear();
    }
    
    
}
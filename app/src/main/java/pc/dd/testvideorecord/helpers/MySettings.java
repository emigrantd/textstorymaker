package pc.dd.testvideorecord.helpers;


import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

/**
 * Created by leaditteam on 20.07.17.
 */

public class MySettings {
    Bitmap backgroundImageChatUrl;
    Boolean conversationTitleOn;
    
    public MySettings(Bitmap backgroundImageChatUrl, Boolean conversationTitleOn) {
        this.backgroundImageChatUrl = backgroundImageChatUrl;
        this.conversationTitleOn = conversationTitleOn;
    }
    
    public Bitmap getBackgroundImageChatUrl() {
        return backgroundImageChatUrl;
    }
    
    public void setBackgroundImageChatUrl(Bitmap backgroundImageChatUrl) {
        this.backgroundImageChatUrl = backgroundImageChatUrl;
    }
    
    public Boolean getConversationTitleOn() {
        return conversationTitleOn;
    }
    
    public void setConversationTitleOn(Boolean conversationTitleOn) {
        this.conversationTitleOn = conversationTitleOn;
    }
}

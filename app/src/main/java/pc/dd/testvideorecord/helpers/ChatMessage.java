package pc.dd.testvideorecord.helpers;

import android.graphics.Bitmap;

/**
 * Created by leaditteam on 11.07.17.
 */


public class ChatMessage {
    public boolean left;
    public String message;
    public String image_url;
    int colorUserMessageText;
    int colorUserBox;
    Boolean isShowAvatar;
   public Bitmap avatarBitmap;
    
    public ChatMessage(boolean left, String message, String image_url, int colorUserMessageText, int colorUserBox, Boolean isShowAvatar, Bitmap avatarBitmap) {
        this.left = left;
        this.message = message;
        this.image_url = image_url;
        this.colorUserMessageText = colorUserMessageText;
        this.colorUserBox = colorUserBox;
        this.isShowAvatar = isShowAvatar;
        this.avatarBitmap = avatarBitmap;
    }
}
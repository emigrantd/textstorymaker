package pc.dd.testvideorecord.helpers;

import android.graphics.Bitmap;

/**
 * Created by leaditteam on 12.07.17.
 */

public class User {
    String name;
    String avatar;
    String sound;
    int colorUserMessageText;
    int colorUserBox;
    Boolean isShowAvatar;
    public Bitmap avatarBitmap;
    public double soundTyping;
    public double soundSendSms;
    
    public User(String name, String avatar, String sound, int colorUserMessageText, int colorUserBox, Boolean isShowAvatar, Bitmap avatarBitmap, double soundTyping, double soundSendSms) {
        this.name = name;
        this.avatar = avatar;
        this.sound = sound;
        this.colorUserMessageText = colorUserMessageText;
        this.colorUserBox = colorUserBox;
        this.isShowAvatar = isShowAvatar;
        this.avatarBitmap = avatarBitmap;
        this.soundTyping = soundTyping;
        this.soundSendSms = soundSendSms;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getAvatar() {
        return avatar;
    }
    
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    
    public String getSound() {
        return sound;
    }
    
    public void setSound(String sound) {
        this.sound = sound;
    }
    
    public int getColorUserMessageText() {
        return colorUserMessageText;
    }
    
    public void setColorUserMessageText(int colorUserMessageText) {
        this.colorUserMessageText = colorUserMessageText;
    }
    
    public int getColorUserBox() {
        return colorUserBox;
    }
    
    public void setColorUserBox(int colorUserBox) {
        this.colorUserBox = colorUserBox;
    }
    
    public Boolean getShowAvatar() {
        return isShowAvatar;
    }
    
    public void setShowAvatar(Boolean showAvatar) {
        isShowAvatar = showAvatar;
    }
    
    public Bitmap getAvatarBitmap() {
        return avatarBitmap;
    }
    
    public void setAvatarBitmap(Bitmap avatarBitmap) {
        this.avatarBitmap = avatarBitmap;
    }
    
    public double getSoundTyping() {
        return soundTyping;
    }
    
    public void setSoundTyping(double soundTyping) {
        this.soundTyping = soundTyping;
    }
    
    public double getSoundSendSms() {
        return soundSendSms;
    }
    
    public void setSoundSendSms(double soundSendSms) {
        this.soundSendSms = soundSendSms;
    }
}

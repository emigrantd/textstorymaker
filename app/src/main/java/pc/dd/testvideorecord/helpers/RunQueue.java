package pc.dd.testvideorecord.helpers;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by leaditteam on 24.07.17.
 */

public class RunQueue implements Runnable {
    private List<Thread> list = new ArrayList<>();
    public int getListCount(){
      return list.size()-1;
    }
    public void queue(Thread task)
    {
        list.add(task);
    }
    
    public void run()
    {
        while(list.size() > 0)
        {
            Thread task = list.get(0);
            
            list.remove(0);
            task.run();
        }
    }
}